import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo1-hello', loadComponent: () => import('./demo/demo1-hello.component')},
  { path: 'demo2-selectors', loadComponent: () => import('./demo/demo2-selectors.component')},
  { path: 'demo3-list', loadComponent: () => import('./demo/demo3-list.component')},
  { path: 'demo4-forms', loadComponent: () => import('./demo/demo4-forms.component')},
  { path: 'demo5-forms', loadComponent: () => import('./demo/demo5-forms.component')},
  { path: 'demo6-forms', loadComponent: () => import('./demo/demo6-forms.component')},
  { path: 'demo7-forms', loadComponent: () => import('./demo/demo7-forms.component')},
  { path: '**', redirectTo: 'demo1-hello'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
