import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    
    <button routerLink="demo1-hello">demo1</button>
    <button routerLink="demo2-selectors">demo2</button>
    <button routerLink="demo3-list">demo3 list</button>
    <button routerLink="demo4-forms">forms 4</button>
    <button routerLink="demo5-forms">forms 5</button>
    <button routerLink="demo6-forms">forms 6</button>
    <button routerLink="demo7-forms">forms 7</button>
    
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
}
