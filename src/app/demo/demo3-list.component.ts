import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MYLIST } from './utils/users-list';

@Component({
  selector: 'app-demo3-list',
  standalone: true,
  imports: [CommonModule],
  template: `
    
    <h1>fabio</h1>
   <div>
      <main>
        <div>
          <article>one</article>
          <article>two</article>
          <article>three</article>
        </div>
      </main>
     
    </div>
   
    <div class="users">
      <li 
        *ngFor="let user of users; let i = index"
        [class]="'item' + i"
      >
        {{user}}
      </li>
    </div>
  `,
})
export default class Demo3ListComponent {
  users = MYLIST;
}
