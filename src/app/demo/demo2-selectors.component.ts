import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo2-selectors',
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>Demo 2 Selectors</h1>
    
    <form class="form-user">
      <h1>User</h1>
      <div>
        <label>Name</label>
        <input type="text"/>
      </div>
    </form>

    <form class="form-car pippo">
      <h1>Car</h1>
      <div>
        <label>Name</label>
        <input type="text"/>
      </div>
    </form>
  `,
})
export default class Demo2SelectorsComponent {

}
