import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo6-forms',
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>Dynamic List</h1>
    <div class="alert" *ngIf="error">ahia!</div>
    
    <ul class="user-list">
      <li *ngFor="let user of users">
        {{user.name}} ({{user.username}})
        <button (click)="deleteUser(user)">delete</button>
      </li>
    </ul>
  `,
})
export default class Demo6FormsComponent {
  http = inject(HttpClient)
  users: User[] = []
  error = false;

  constructor() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe({
        next: (res) => this.users = res,
        error: () => this.error = true
      })
  }
  deleteUser(user: User) {
    this.http.delete('https://jsonplaceholder.typicode.com/users/'+user.id)
      .subscribe({
        next: () => this.users = this.users.filter(u => u.id !== user.id),
        error: () => this.error = true
      })
  }
}


export interface User {
  id: number;
  name: string;
  username: string;
}
