import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-demo5-forms',
  standalone: true,
  imports: [CommonModule, FormsModule],
  template: `
    <h1>Demo form</h1>

    <div class="alert" *ngIf="success">Ok. Sent!</div>
    <form 
      #f="ngForm"
      style="display: flex; flex-direction: column; gap: 10px; margin: 10px"
      (submit)="save()"
    >
      
      <div>
        <div>First Name</div>
        <input
          type="text"
          name="firstName"
          value="hello"
          ngModel
          required
          minlength="3"
        />
      </div>
  
      <input
        type="checkbox"
        name="subscribe"
        [checked]="true"
      />
  
      <input
        type="date"
        name="date"
        [value]="today"
        ngModel
        required
      />
  
      <select
        name="food"
      >
        <option value="">Select something</option>
        <option value="1">Milk</option>
        <option value="2">Nutella</option>
        <option value="3">Bread</option>
      </select>
  
      
      <button [disabled]="f.invalid">SUBMIT</button>
    </form>
  `,
  styles: [
  ]
})
export default class Demo5FormsComponent {
  today = new Date().toISOString().slice(0, 10);
  success = false

  save() {
    this.success = true
  }
}
