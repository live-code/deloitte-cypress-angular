import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo1-hello',
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>Demo 1 Hello</h1>
    <h1 class="hello">two</h1>
    <h1 data-testid="hello">three</h1>
    <div style="font-size: 10px">four</div>
    
    <ul>
      <li>item</li>
      <li>item</li>
      <li>item</li>
      <li>item</li>
      <li>item</li>
    </ul>
    
    <form class="main-form">
      <div class="form-group inline">
        <label>First Name</label>
        <input type="text">
      </div>
      
      <div *ngIf="msg">{{msg}}</div>
      <div class="form-group multiline">
        <button>1</button>
        <label>Last Name</label>
        <button>2</button>
        <input type="text" 
               (focus)="setFocus()"
               (blur)="msg = ''"
        >
        <input type="text">
      </div>
      
      <div class="form-group multiline">
        <label>Email</label>
        <input type="text">
      </div>
    </form>
  `,
})
export default class Demo1HelloComponent {
  msg = ''

  setFocus() {
    this.msg = 'input selected'
  }
}
