import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo4-forms',
  standalone: true,
  imports: [CommonModule],
  template: `

    <button
      (mouseover)="visible = true"
      (mouseout)="visible = false"
    >TOGGLE</button>

    <div
      class="alert" 
      *ngIf="visible"
    >Successfully done</div>
    
  `,
})
export default class Demo4FormsComponent {
  visible = false;
}
