import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo7-forms',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      demo7-forms works!
    </p>
  `,
  styles: [
  ]
})
export default class Demo7FormsComponent {

}
