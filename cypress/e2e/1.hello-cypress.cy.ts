describe('Demo 1 Page', function () {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo1-hello')

    cy.contains('Last Name')
      .siblings('input')
      .first()
      .as('firstInputLastName')
  })

  it('should display the title', function () {
    cy.get('h1').contains('Demo 1 Hello')
  });

  it('should open the page', function () {
    cy.get('h1').contains('two').should('have.class','hello')
    cy.get('div').contains('four').should('have.css','fontSize', '10px')
  });


  it('the list should display 5 items ', function () {
    cy.get('li')
      .should('have.length', 5)

    cy.get('ul')
      .children()
      .should('have.length', 5)
  })

  it('should first name form  contains 2 children', function () {
    cy.contains('First Name')
      .parent()
      .children()
      .should('have.length', 2)
  });

  it('last name should be multiline ', function () {
    cy.contains('Last Name')
      .parent()
      .should('have.class', 'multiline')
  });


  it('last name should be multiline ', function () {
    cy.contains('Last Name')
      .parent()
      .should('have.class', 'multiline')
  });

  it('select parent form ', function () {
    cy.contains('First Name')
      .closest('form')
      .should('have.class', 'main-form')
  });

  it('should display a msg when LastName is focused', function () {
    cy.get('@firstInputLastName')
      .focus()

    cy.contains('input selected')
      .should('be.visible')
  });

  it('should hide the msg when LastName is blurred', function () {
    cy.get('@firstInputLastName').focus()
    cy.get('@firstInputLastName').blur()

    cy.contains('input selected')
      .should('not.exist')
  });

});















