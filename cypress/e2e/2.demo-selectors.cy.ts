import { visit } from '@angular/compiler-cli/src/ngtsc/util/src/visitor';

describe('Demo 2 selectors', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo2-selectors')
  })

  it('select user form input ', function () {
    cy.get('.form-user input')
      .should('be.visible')

    cy.get('.form-user input')
      .type('Fabio')
  });

  it('select car form input ', function () {
    cy.get('.form-car')
      .within(() => {
        cy.get('input')
          .should('be.visible')

        cy.get('input')
          .type('Fiat 127')
      })


  });

})
