import { User } from '../../src/app/demo/demo6-forms.component';

const mocklist: User[] = [
  { id: 15, name: 'Fabio', username: 'fabione'},
  { id: 2, name: 'Ciccio', username: 'cicciox'},
  { id: 3, name: 'Ciccio', username: 'cicciox'},
  { id: 4, name: 'Ciccio', username: 'cicciox'},
  { id: 5, name: 'Ciccio', username: 'cicciox'},
]

describe('list HTTP example with success' ,() => {
  beforeEach(() => {
    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET'},
      mocklist
    )

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users/1',
      { method: 'DELETE'},
      {
        statusCode: 200,
      }
    )
    cy.visit('http://localhost:4200/demo6-forms')

    cy.get('.user-list').children().as('listItems')
  })

  it('should display all items', function () {
    cy.get('@listItems')
      .should('have.length', mocklist.length)
  });

  it('should display the first element', function () {
    cy.get('@listItems')
      .first()
      .should('contain', mocklist[0].name)
      .and('contain', mocklist[0].username)
  });

  it('should display the last element', function () {
    cy.get('@listItems')
      .last()
      .should('contain', mocklist[mocklist.length -1 ].name)
  });

  it(`should all items display its own name and username`, () => {
    cy.get('@listItems')
      .each(($el, index) => {
        cy.wrap($el).contains(mocklist[index].name)
        cy.wrap($el).contains(mocklist[index].username)
      })
  });

  it('should delete an element', function () {
    cy.get('.user-list')
      .children()
      .first()
      .contains('delete')
      .click();

    cy.get('.user-list')
      .children()
      .should('have.length', mocklist.length - 1)

    cy.get('.user-list')
      .children()
      .first()
      .contains(mocklist[0].name)
      .should('not.exist')
  });

})

describe('list GET  http with errors', () => {
  beforeEach(() => {
    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET'},
      {
        statusCode: 404,
        body: 'endpoint not available'
      }
    )
    cy.visit('http://localhost:4200/demo6-forms')
  })

  it('should display an error if HTTP fails', function () {
    cy.get('.alert')
      .contains('ahia!')
  });

})

describe('list http DELETE/POST/PATCH with errors', () => {
  beforeEach(() => {
    cy.intercept(
      'https://jsonplaceholder.typicode.com/users',
      { method: 'GET'},
      mocklist
    )

    cy.intercept(
      'https://jsonplaceholder.typicode.com/users/**',
      { method: 'DELETE'},
      {
        statusCode: 404,
        body: 'ok'
      }
    )

    cy.visit('http://localhost:4200/demo6-forms')

  })

  it('should display an error if DELETE fails', function () {
    cy.get('.user-list')
      .children()
      .first()
      .get('button')
      .contains('delete')
      .click();

    cy.get('.alert')
      .contains('ahia!')
  });

})
