import { MYLIST } from '../../src/app/demo/utils/users-list';

describe('Playground', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo3-list')
  })

  it('should display 3 articles', function () {
    cy.get('article')
      .should('have.length', 3)

    cy.get('main article')
      .should('have.length', 3)

    cy.get('main')
      .find('article')
      .should('have.length', 3)
  });

  it('should first item be "one"', function () {
    cy.get('main')
      .find('article')
      .first()
      .contains('one')
  });

  it('should middle item be "two"', function () {
    cy.get('main')
      .find('article')
      .eq(1)
      .contains('two')
  });

  it('should last item be "three"', function () {
    cy.get('main')
      .find('article')
      .last()
      .contains('three')
  });

  it('users list should display users (bad solution)', function () {
    MYLIST.forEach(item => {
      cy.get('.users').contains(item)
    })
  });

  it('users list should display users (real data)', function () {

    MYLIST.forEach((item, index) => {
      cy.get('.users')
        .find('li')
        .eq(index)
        .contains(MYLIST[index])
    })
  });

  it('users list should display users (real data)', function () {
    MYLIST.forEach((item, index) => {
      cy.get('.users')
        .find('li')
        .eq(index)
        .should('have.class', 'item' + index)
    })
  });

  it.only('users list should display users (real data)', function () {

    cy.get('.users')
      .find('li')
      .each(($item, index) => {
        cy.wrap($item)
          .should('have.class', 'item' + index)

        cy.wrap($item)
          .contains(MYLIST[index])
      })

  });


})
