describe('demo 4 forms', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo4-forms')
  })

  it('should hide msg when page is mount ', () => {
    cy.contains('Success').should('not.exist');
  });

  it.only('should show msg if clicked ', () => {
    cy.get('button')
      .contains('TOGGLE')
      .trigger('mouseover');

    cy.get('.alert').contains('Success')
  });


  it('should hide msg if clicked twice ', () => {
    cy.get('button').contains('TOGGLE').as('btn');
    cy.get('@btn').trigger('mouseover')
    cy.get('@btn').trigger('mouseout')

    cy.contains('Success')
      .should('not.exist')
  });
})
