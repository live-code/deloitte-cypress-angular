describe('form example' ,() => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/demo5-forms')

    cy
      .get('button')
      .contains('submit', { matchCase: false})
      .as('submitBtn')

  })

  it('submit btn should be disable by default', function () {
    cy.get('@submitBtn')
      .should('be.disabled')
  });


  it('button should be enable if validation passes', function () {
    cy
      .contains('First Name')
      .siblings('input')

    cy
      .get('input[name="firstName"]')
      .clear()
      .type('lorenzo')

    cy
      .get('input[name="subscribe"]')
      .uncheck()

    cy
      .get('input[name="date"]')
      .type('2011-03-21')

    cy.get('select[name="food"]')
      .select('Nutella')
      // .select(3)

    cy.get('@submitBtn')
      .should('be.enabled')
  });

  it('button should be disabled if name is invalid', function () {
    cy
      .get('input[name="firstName"]')
      .clear()
      .type('lo')

    cy
      .get('input[name="date"]')
      .type('2011-03-21')

    cy.get('@submitBtn')
      .should('be.disabled')
  });

  it('button should be disabled if date is invalid', function () {
    cy
      .get('input[name="firstName"]')
      .clear()
      .type('lorenzo')

    cy.get('@submitBtn')
      .should('be.disabled')
  });

  it('show msg when form is submitted', function () {
    cy
      .get('input[name="firstName"]')
      .clear()
      .type('lorenzo')

    cy
      .get('input[name="date"]')
      .type('2011-03-21')

    cy.get('@submitBtn')
      .click()

    cy.get('.alert')
      .contains('Ok')
  });
})
